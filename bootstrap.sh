#!/bin/bash

set -e
set -u

function pman {
  pacman --noconfirm $@
}

function update_cache {
  pman -Sy
}

function install_salt {
  pman -S salt-zmq
  mkdir -p /etc/salt/minion.d
  echo 'file_client: local' > /etc/salt/minion.d/file_client_local.conf
}

function install_unzip {
  pman -S unzip
}

function get_repo {
  wget https://bitbucket.org/panifex/soda/get/1e8ee0c7530a.zip
  mkdir -p /srv/salt
  unzip *.zip -d /srv/salt
}

update_cache
install_salt
install_unzip
get_repo
