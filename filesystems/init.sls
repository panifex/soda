/dev/sda2:
  lvm.pv_present

vg:
  lvm.vg_present:
    - devices: /dev/sda2

boot:
  lvm.lv_present:
    - vgname: vg
    - size: 200M

swap:
  lvm.lv_present:
   - vgname: vg
   - size: 512M
